#!/usr/bin/env sh

getpython() {
    # Different distros name their pythons differently. Get the first available.
    for p in python36 python37 python38 python3 python; do
        if which "$p" 2>/dev/null; then return; fi
    done
}

python="$(getpython)"

"$python" urlheadcheck/uhcmain.py
