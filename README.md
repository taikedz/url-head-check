# URL 'HEAD' checker

Small utility to regularly check URLs using HTTP `HEAD` requests and report to a log.

# Usage

You need Python 3 installed with the libraries `yaml` and `requests`. You may need to run `pip install -r requirements.txt`

## Configure `urlheadcheck.yaml`

You can see example configurations in `examples/`

Set `sleep` to something sensible, like `60` to run the check every 60 seconds

Set `urls` to a list of URL configurations, with these properties:

    * `link` - the URL to check. Required.
    * `method` - optional. Can be `GET` or `HEAD`. Default is `HEAD`
    * `expect` - a list of statuses or status classes to expect as valid. Default is `[100, 200, 300]`

Set `log` to have two properties:

    * `file` the name of the file to write logging to
    * `level` the logging level, debug, info, warning, error, critical, fatal

## Run it

On Windows, open a `powershell.exe` session in this folder, and type

```bat
.\run
```

On Linux open a terminal session in this folder, and type

```sh
./run.sh
```

### Codes

Successful checks do not get reported, unless in debug mode.

Problems reported are pritned to the log, and to stdout, with a code. Codes of 100 and higher are HTTP status codes

Codes less than 100 are program errors:

* `1` - Generic error
* `10` - SSL error - invalid certificate
