#!/usr/bin/env python3

import time
import datetime
import numbers
from os import linesep as crlf
import logging
import requests

import config
import urlcheck

conffilename = "urlheadcheck.yaml"
confdata = None

class HaltingException(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)

def setuplogging(logconf):
    loglevels = {
        "debug": logging.DEBUG,
        "info": logging.INFO,
        "warning": logging.WARNING,
        "error": logging.ERROR,
        "critical": logging.CRITICAL,
        "fatal": logging.FATAL,
    }

    logging.basicConfig(
        filename=logconf['file'],
        filemode="a",
        level=loglevels[logconf['level']],
        format="%(asctime)s %(levelname)s : %(message)s"
        )

def main():
    global confdata
    global conffilename

    requirements = {
        "sleep": numbers.Number,
        "urls": list,
        "log": dict,
    }

    try:
        confdata = config.load(conffilename, require=requirements)
        setuplogging(confdata['log'])

        adjustUrlTypes(confdata)

        urllist = []
        for item in confdata['urls']:
            urllist.append(item['link'])

        urls_to_check = "Checking URLs:"+crlf+"\t"+(crlf+"\t").join(urllist)
        print(getdate() + crlf + urls_to_check)
        logging.info(urls_to_check)

    except FileNotFoundError as e:
        logging.debug(e)
        raise HaltingException("Could not get config file "+conffilename) from e

    except TypeError as e:
        logging.debug(e)
        raise HaltingException("Program error. Enable debug mode for more info.") from e

    except Exception as e:
        logging.debug(e)
        raise HaltingException("Problem with config file: "+str(e))

    while True:
        runcheck()

        logging.debug("(sleep %s sec)"%(confdata['sleep']))
        time.sleep(confdata['sleep'])


def adjustUrlTypes(yamldata):
    for urlitem in yamldata['urls']:
        urlitemkeys = urlitem.keys()

        if 'link' not in urlitemkeys:
            raise KeyError("'link' property required for url item : %s"% str(urlitem))

        if 'expect' not in urlitemkeys:
            urlitem['expect'] = [100, 200, 300]

        if 'method' not in urlitemkeys:
            urlitem['method'] = "HEAD"

        if urlitem['method'] == "HEAD":
            urlitem['method'] = requests.head
        elif urlitem['method'] == "GET":
            urlitem['method'] = requests.get
        else:
            raise ValueError("%s is not a supported method"%(urlitem['method']))

def getdate():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

def runcheck():
    for urlitem in confdata['urls']:
        success, status = urlcheck.reachable(urlitem)

        if success:
            logging.debug("OK - %s - %s" % ( status, urlitem['link']) )

        else:
            message = "-- URL check failure: %s : %s"%(status, urlitem['link'])
            rundate = getdate()
            print(rundate + " " + message) # the log may be cluttered, print also to tty for convenience
            logging.warning(message)


while True:
    try:
        main()

    except KeyboardInterrupt as e:
        logging.info("Exited via interrupt")
        print("Goodbye")
        exit(0)

    except HaltingException as e:
        logging.error(str(e) )
        print(e)
        exit(11)

    except Exception as e:
        logging.warning("Non-halting exception")
        logging.warning(str(e) )
        print(e)
        #exit(1) # during testing, comment out in prod
