import sys

if sys.version_info.major < 3:
    print("You need Python 3 to run this script.")
    exit(1)

import urlheadcheck
