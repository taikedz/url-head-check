import requests
import logging
import os

ERROR_GENERIC = 1
ERROR_SSL = 10

def reachable(urlitem, sslcheck=True):
    try:
        url = urlitem['link']
        method = urlitem['method']
        response = method(url, verify=sslcheck)
        status = response.status_code

        if (status in urlitem['expect'] or
            (status - status % 100) in urlitem['expect']):

            return True, status

        else:
            return False, status

    except requests.exceptions.SSLError as e:
        logging.debug(str(e))
        logging.error("SSL error")
        return False, ERROR_SSL

    except Exception as e:
        logging.debug(str(e))
        logging.error(strargs(e))
        return False, ERROR_GENERIC

def strargs(e):
    myargs = []
    for item in e.args:
        myargs.append( str(item) )

    return os.linesep.join(myargs)

if __name__ == "__main__":
    from sys import argv
    
    for url in argv[1:]:
        print("%s : %s" % (str(reachable(url, False) ), url) )
