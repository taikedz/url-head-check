import yaml

def load(filename, require):
    yamldata = None

    with open(filename) as fh:
        yamldata = yaml.load(fh, Loader=yaml.SafeLoader)

    yamlkeys = yamldata.keys()

    for key in yamldata:
        if key in yamlkeys:
            if not (isinstance(yamldata[key], require[key])):
                raise ValueError(
                    "Value for key '%s' is type '%s' (expected '%s')" %
                    (
                        key,
                        type(yamldata[key]),
                        require[key],
                    )
                )
        else:
            raise ValueError(
                "Missing %s from YAML config (should be of type '%s')" %
                (key, require[key])
            )

    return yamldata

if __name__ == "__main__":
    print(
        load("urlheadcheck.yaml", require={
            "urls" : list,
            "sleep": int,
        })
    )
